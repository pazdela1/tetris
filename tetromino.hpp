#ifndef TETRIS_TETROMINO_HPP
#define TETRIS_TETROMINO_HPP

#include <vector>
#include <algorithm>
#include <iostream>

enum class tetris_block
{
    EMPTY,
    FULL
};

// https://qph.cf2.quoracdn.net/main-qimg-356e2b21c801381db2890dab49a9ea88
enum class tetromino_type
{
    I_BLOCK,
    J_BLOCK,
    L_BLOCK,
    O_BLOCK,
    S_BLOCK,
    T_BLOCK,
    Z_BLOCK
};

class tetromino
{
private:
    std::vector<std::vector<tetris_block>> shape;
    tetromino_type type;
    int board_x, board_y;
    int width{}, height{};

    void set_dimensions();

    void set_starting_position();

    void initialize_shape();

public:
    explicit tetromino(tetromino_type t) : type(t), board_x(0), board_y(0)
    {
        set_dimensions();
        set_starting_position();
        shape = std::vector<std::vector<tetris_block>>(height, std::vector<tetris_block>(width, tetris_block::EMPTY));
        initialize_shape();
    }

    void rotate();

    void rotate_back();

    void display_tetromino() const;

    const std::vector<std::vector<tetris_block>> &get_shape() const;

    tetromino_type get_type() const;

    int get_board_x() const;
    void set_board_x(int new_x);

    int get_board_y() const;
    void set_board_y(int new_y);

    int get_width() const;
    int get_height() const;
};

#endif // TETRIS_TETROMINO_HPP
