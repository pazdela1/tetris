#include "tetris_board.hpp"

// __________TETROMINO_DEFINITIONS__________

tetris_board::tetris_board() : rows(BOARD_HEIGHT), cols(BOARD_WIDTH), board(BOARD_HEIGHT, std::vector<tetris_block>(BOARD_WIDTH, tetris_block::EMPTY)) {}

// Place a block at a specific position
void tetris_board::place_block(int row, int col)
{
    if (row >= 0 && row < rows && col >= 0 && col < cols)
    {
        board[row][col] = tetris_block::FULL;
    }
}

// Check if a cell is empty
bool tetris_board::is_cell_empty(int row, int col) const
{
    if (row < 0 || row >= board.size() || col < 0 || col >= board[0].size())
    {
        return false;
    }
    return board[row][col] == tetris_block::EMPTY;
}

// Check if a line is full
bool tetris_board::full_line(int row) const
{
    for (auto block : board[row])
    {
        if (block != tetris_block::FULL)
        {
            return false;
        }
    }
    return true;
}

// Clear a line if it is full
void tetris_board::clear_line(int row)
{
    for (int col = 0; col < cols; ++col)
    {
        board[row][col] = tetris_block::EMPTY;
    }
}

// Clear all lines that are full
int tetris_board::clear_full_lines()
{
    int cleared_lines_count = 0;
    for (int row = 0; row < rows; ++row)
    {
        if (full_line(row))
        {
            cleared_lines_count++;
            clear_line(row);
            // Shift down all lines above the cleared line
            for (int r = row; r > 0; --r)
            {
                board[r] = board[r - 1];
            }
            board[0] = std::vector<tetris_block>(cols, tetris_block::EMPTY);
        }
    }

    // Calculate the score based on the number of lines cleared

    int score = 10;
    switch (cleared_lines_count)
    {
    case 1:
        score = 100;
        break;
    case 2:
        score = 300;
        break;
    case 3:
        score = 500;
        break;
    case 4:
        score = 800;
        break;
    default:
        break; // No score for clearing 0 or more than 4 lines
    }

    return score;
}

// Display the board state
void tetris_board::display_board() const
{
    for (const auto &row : board)
    {
        for (auto cell : row)
        {
            if (cell == tetris_block::FULL)
            {
                std::cout << "[X]"; // Filled cell
            }
            else
            {
                std::cout << "[ ]"; // Empty cell
            }
        }
        std::cout << std::endl;
    }
}

const std::vector<std::vector<tetris_block>> &tetris_board::get_board() const { return board; }

int tetris_board::get_rows() const { return rows; }

int tetris_board::get_cols() const { return cols; }
