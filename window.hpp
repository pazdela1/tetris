#ifndef TETRIS_WINDOW_HPP
#define TETRIS_WINDOW_HPP

#include <sstream>

#include "tetris_board.hpp"

#define ANSI_CLEAR "\x1B[2J\x1B[H"
#define ANSI_COLOR_RESET "\x1B[m"
#define COLOR_RED "\x1B[91m"
#define COLOR_GREEN "\x1B[92m"
#define COLOR_ORANGE "\x1B[48;5;52m\x1B[38;5;208m"
#define COLOR_GAME_OVER "\x1B[48;5;17m\x1B[38;5;75m"

class Window
{
public:
    explicit Window(std::ostream &outputStream, int width, int height);
    ~Window(){};

    void redraw(std::vector<std::vector<tetris_block>> board, const tetromino &current_tetromino, int score, bool game_state);

    void redraw_end_of_game(bool victory, int final_score);

private:
    std::ostream &outputStream;
    std::string text;
    std::string score;
    int width, height;
};

#endif // TETRIS_WINDOW_HPP
