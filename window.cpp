#include "window.hpp"

Window::Window(std::ostream &outputStream, int width, int height) : outputStream(outputStream),
                                                                    width(width),
                                                                    height(height) {}

bool is_tetromino_cell(int row, int col, const tetromino &current_tetromino)
{
    int tetrominoX = current_tetromino.get_board_x();
    int tetrominoY = current_tetromino.get_board_y();
    const auto &shape = current_tetromino.get_shape();

    int localX = col - tetrominoX;
    int localY = row - tetrominoY;

    if (localX >= 0 && localX < current_tetromino.get_width() &&
        localY >= 0 && localY < current_tetromino.get_height())
    {
        return shape[localY][localX] == tetris_block::FULL;
    }
    return false;
}

void Window::redraw(std::vector<std::vector<tetris_block>> board, const tetromino &current_tetromino, int score, bool game_state)
{
    // Clear the screen and reset the text
    std::stringstream ss;
    ss << ANSI_CLEAR << ANSI_COLOR_RESET;

    ss << COLOR_GREEN;
    ss << "     _____    _        _      \r" << std::endl;
    ss << "    |_   _|__| |_ _ __(_)___  \r" << std::endl;
    ss << "      | |/ _ \\ __| '__| / __| \r" << std::endl;
    ss << "      | |  __/ |_| |  | \\__ \\ \r" << std::endl;
    ss << "      |_|\\___|\\__|_|  |_|___/ \r" << std::endl
       << std::endl;
    ss << ANSI_COLOR_RESET;

    ss << COLOR_ORANGE << "==================================" << ANSI_COLOR_RESET << "\r" << std::endl;

    // Draw the Tetris board
    for (int row = 0; row < board.size(); ++row)
    {
        ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET;

        for (int col = 0; col < board[row].size(); ++col)
        {
            bool tetromino_cell = is_tetromino_cell(row, col, current_tetromino);

            if (board[row][col] == tetris_block::FULL || tetromino_cell)
            {
                ss << "[" << COLOR_RED << "X" << ANSI_COLOR_RESET << "]"; // Representing a filled cell
            }
            else
            {
                ss << "[ ]"; // Representing an empty cell
            }
        }

        ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET << "\r" << std::endl;
    }

    ss << COLOR_ORANGE << "==================================" << ANSI_COLOR_RESET << "\r" << std::endl;
    ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET << "                              " << COLOR_ORANGE << "||" << ANSI_COLOR_RESET << "\r" << std::endl;

    std::string scoreStr = std::to_string(score);
    int totalLength = 34; // Desired total length
    int scoreLength = scoreStr.length();
    int spacesNeeded = totalLength - 20 - scoreLength; // "   SCORE: " is 11 characters long

    ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET;
    ss << "         SCORE: ";
    ss << scoreStr;
    for (int i = 0; i < spacesNeeded; ++i)
    {
        ss << " ";
    }
    ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET;
    ss << "\r" << std::endl;

    ss << COLOR_ORANGE << "||" << ANSI_COLOR_RESET << "                              " << COLOR_ORANGE << "||" << ANSI_COLOR_RESET << "\r" << std::endl;
    ss << COLOR_ORANGE << "==================================" << ANSI_COLOR_RESET << "\r" << std::endl;

    if (game_state)
    {
        ss << COLOR_GREEN;
        ss << "  ____                         ___                 \r" << std::endl;
        ss << " / ___| __ _ _ __ ___   ___   / _ \\__   _____ _ __ \r" << std::endl;
        ss << "| |  _ / _` | '_ ` _ \\ / _ \\ | | | \\ \\ / / _ \\ '__|\r" << std::endl;
        ss << "| |_| | (_| | | | | | |  __/ | |_| |\\ V /  __/ |   \r" << std::endl;
        ss << " \\____|\\__,_|_| |_| |_|\\___|  \\___/  \\_/ \\___|_|   \r" << std::endl
           << std::endl;
        ss << ANSI_COLOR_RESET;
    }

    // Output the complete frame
    outputStream << ss.str();
}
