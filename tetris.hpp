#ifndef TETRIS_TETRIS_HPP
#define TETRIS_TETRIS_HPP

#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <random>
#include <mutex>
#include <condition_variable>

#include "window.hpp"

#define DEFAULT_SPEED 1000

class tetris
{
private:
    tetris_board board;
    tetromino current_tetromino;
    int width, height;
    bool game_over;

    int game_speed;
    int score;

    std::clock_t timer;
    std::mt19937 rng;

    std::unique_ptr<Window> window;
    std::mutex mutex;
    std::condition_variable condition_variable;

    tetromino create_random_tetromino();

    bool is_out_of_bounds() const;
    void adjust_position_in_bounds();

    bool is_colliding() const;
    bool check_collision(int new_x, int new_y) const;

    bool can_move_down();

    void merge_to_board(const std::vector<std::vector<tetris_block>> &shape, int x, int y);
    void lock_tetromino();

public:
    tetris();
    void update();
    void output();
    void input();

    void move_left();
    void move_right();
    void move_down();
    void rotate();

    void display_game_state() const;

    std::mutex &get_mutex();
    std::condition_variable &get_condition_variable();

    void place_block(int row, int col);

    bool is_game_over() const;
    void set_game_over(bool game_state);

    int get_game_speed() const;
    void set_game_speed(int game_speed);
};

#endif // TETRIS_TETRIS_HPP
