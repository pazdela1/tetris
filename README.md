# Tetris

# Vzor
https://cs.wikipedia.org/wiki/Tetris

# Zadání
Implementujte hru Tetris. Jednotlivé dílky se budou generovat náhodně. Hráč bude hru ovládát pomocí kláves. Hra se bude v reálném čase zobrazovat na terminál. Hráč bude získávat skóre pomocí skládání dílků do řad. Hra bude končit, pokud nelze vytvořit nový dílek, nebo pokud hráč ukončí hru klávesou. Na konci se hry se zobrazí finální skóre.

# Kompilace programu
Program lze zkompilovat následujícím příkazem:
```bash
cmake -Bcmake-build-debug -H. -DCMAKE_BUILD_TYPE=Debug
cmake --build cmake-build-debug
```

# Spuštění a přepínače
Hra se spouští z příkazové řádky. Při spouštění hry lze nastavit obtížnost. Hra bude rychlejší nebo pomalejší podle nastavené obtížnosti.

Příklady spuštění programu:
```bash
clear
./tetris
./tetris --help
./tetris --easy
./tetris --medium
./tetris --hard
./tetris --impossible
valgrind ./tetris
```

# Ovládání programu
Hra se ovládá následujícími klávesami:

`A` - posunutí tetromina doleva
`D` - posunutí tetromina doprava
`S` - posunutí tetromina dolů
`W` - rotace tetromina po směru hodinových ručiček

# Ukončení programu
Program je ukončí automaticky, pokud se nemůže vytvořit nové tetromino.

Program je možné kdykoliv ukončit klávesou `Q`.
