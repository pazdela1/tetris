#include "tetris.hpp"

// __________TETRIS_GAME_DEFINITIONS__________

// Constructor
tetris::tetris() : board(), current_tetromino(static_cast<tetromino_type>(rand() % 7)), width(BOARD_WIDTH), height(BOARD_HEIGHT), score(0), timer(std::clock())
{
    game_over = false;
    game_speed = DEFAULT_SPEED;
    // Initialize the random seed
    std::random_device rd;
    rng = std::mt19937(rd());

    window = std::make_unique<Window>(std::cout, width, height);
    window->redraw(board.get_board(), current_tetromino, score, game_over);
}

// Random tetromino generation
tetromino tetris::create_random_tetromino()
{
    std::uniform_int_distribution<int> dist(0, 6); // Range for tetromino_type values
    auto random_type = static_cast<tetromino_type>(dist(rng));
    return tetromino(random_type);
}

// Check if any part of the tetromino is out of bounds
bool tetris::is_out_of_bounds() const
{
    int x = current_tetromino.get_board_x();
    int y = current_tetromino.get_board_y();
    const auto &shape = current_tetromino.get_shape();

    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL)
            {
                if (x + j < 0 || x + j >= board.get_cols() || y + i >= board.get_rows())
                {
                    return true;
                }
            }
        }
    }
    return false;
}

// Adjust the tetromino position to bring it back in bounds
void tetris::adjust_position_in_bounds()
{
    while (current_tetromino.get_board_x() + current_tetromino.get_width() > board.get_cols())
    {
        current_tetromino.set_board_x(current_tetromino.get_board_x() - 1);
    }
    while (current_tetromino.get_board_x() < 0)
    {
        current_tetromino.set_board_x(current_tetromino.get_board_x() + 1);
    }
    while (current_tetromino.get_board_y() + current_tetromino.get_height() > board.get_rows())
    {
        current_tetromino.set_board_y(current_tetromino.get_board_y() - 1);
    }
}

// Check if the tetromino is colliding with any blocks on the board
bool tetris::is_colliding() const
{
    int x = current_tetromino.get_board_x();
    int y = current_tetromino.get_board_y();
    const auto &shape = current_tetromino.get_shape();

    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL && !board.is_cell_empty(y + i, x + j))
            {
                return true;
            }
        }
    }
    return false;
}

// Check if the tetromino is colliding with any blocks on the board or if it is out of bounds
bool tetris::check_collision(int new_x, int new_y) const
{
    const std::vector<std::vector<tetris_block>> &shape = current_tetromino.get_shape();

    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL)
            {
                int check_x = new_x + j;
                int check_y = new_y + i;

                if (check_x < 0 || check_x >= board.get_cols() || check_y < 0 || check_y >= board.get_rows())
                {
                    return true; // Collision with the board boundaries
                }

                if (!board.is_cell_empty(check_y, check_x))
                {
                    return true; // Collision with other blocks
                }
            }
        }
    }
    return false; // No collision
}

// Check if the tetromino can move down
bool tetris::can_move_down()
{
    const std::vector<std::vector<tetris_block>> &shape = current_tetromino.get_shape();
    int tetromino_x = current_tetromino.get_board_x();
    int tetromino_y = current_tetromino.get_board_y();

    // Find the lowest filled cell in the Tetromino's shape
    int max_filled_y = 0;
    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL)
            {
                max_filled_y = std::max(max_filled_y, i);
            }
        }
    }

    // Collision with the board boundaries
    if (tetromino_y + max_filled_y + 1 >= board.get_rows())
    {
        return false;
    }

    /*
    // Collision with the board boundaries
    if (tetromino_y + shape.size() > board.get_rows())
    {
        return false;
    }
    */

    // Collision with other blocks
    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL && !board.is_cell_empty(tetromino_y + i + 1, tetromino_x + j))
            {
                return false;
            }
        }
    }

    return true;
}

// Merge the tetromino to board
void tetris::merge_to_board(const std::vector<std::vector<tetris_block>> &shape, int x, int y)
{
    for (int i = 0; i < shape.size(); ++i)
    {
        for (int j = 0; j < shape[i].size(); ++j)
        {
            if (shape[i][j] == tetris_block::FULL)
            {
                if (board.is_cell_empty(y + i, x + j))
                {
                    board.place_block(y + i, x + j);
                }
            }
        }
    }
}

// Lock the tetromino by merging it to the board
void tetris::lock_tetromino()
{
    const std::vector<std::vector<tetris_block>> &shape = current_tetromino.get_shape();
    int x = current_tetromino.get_board_x();
    int y = current_tetromino.get_board_y();

    merge_to_board(shape, x, y);
}

// Main game loop method
// Move the tetromino down by 1 if it can or create a new one
void tetris::update()
{
    std::unique_lock<std::mutex> lock(mutex);

    if (!can_move_down())
    {
        // Lock the tetromino if it cannot move down, clear full lines and create a new tetromino
        lock_tetromino();
        score += board.clear_full_lines();
        current_tetromino = create_random_tetromino();

        // Check if the new tetromino cannot move down
        if (!can_move_down())
        {
            game_over = true;
        }
    }
    else
    {
        // Move the tetromino down by 1 if it can
        current_tetromino.set_board_y(current_tetromino.get_board_y() + 1);
    }
    condition_variable.notify_all();
}

// Output the current game state to the window
void tetris::output()
{
    std::unique_lock<std::mutex> lock(mutex);
    condition_variable.wait(lock);
    window->redraw(board.get_board(), current_tetromino, score, game_over);
}

// Take the user input and act accordingly
void tetris::input()
{
    std::unique_lock<std::mutex> lock(mutex);
    lock.unlock();

    char c;
    std::cin >> c;

    switch (c)
    {
    case 'a':
        move_left();
        break;
    case 'd':
        move_right();
        break;
    case 'w':
        rotate();
        break;
    case 's':
        move_down();
        break;
    case 'g':
        set_game_over(true);
        break;
    default:
        break;
    }

    // Notify other threads if necessary
    condition_variable.notify_all();
}

// Move the tetromino left by 1 block if possible
void tetris::move_left()
{
    int new_x = current_tetromino.get_board_x() - 1;
    int current_y = current_tetromino.get_board_y();

    if (!check_collision(new_x, current_y))
    {
        current_tetromino.set_board_x(new_x);
    }
}

// Move the tetromino right by 1 block if possible
void tetris::move_right()
{
    int new_x = current_tetromino.get_board_x() + 1;
    int current_y = current_tetromino.get_board_y();

    if (!check_collision(new_x, current_y))
    {
        current_tetromino.set_board_x(new_x);
    }
}

// Move the tetromino down by 1 block if possible
void tetris::move_down()
{
    int current_x = current_tetromino.get_board_x();
    int new_y = current_tetromino.get_board_y() + 1;

    if (!check_collision(current_x, new_y))
    {
        current_tetromino.set_board_y(new_y);
    }
}

// Rotate the tetromino if possible
void tetris::rotate()
{
    // Save the original position
    int original_x = current_tetromino.get_board_x();
    int original_y = current_tetromino.get_board_y();

    current_tetromino.rotate();

    // Check and adjust if the shape is out of bounds
    if (is_out_of_bounds())
    {
        adjust_position_in_bounds();
    }

    // Adjust back to original position if colliding
    if (is_colliding())
    {
        current_tetromino.rotate_back();
        current_tetromino.set_board_x(original_x);
        current_tetromino.set_board_y(original_y);
    }
}

// Display the locked board and the current tetromino on the board
void tetris::display_game_state() const
{
    const std::vector<std::vector<tetris_block>> &shape = current_tetromino.get_shape();
    int tetromino_x = current_tetromino.get_board_x();
    int tetromino_y = current_tetromino.get_board_y();

    for (int row = 0; row < board.get_rows(); ++row)
    {
        for (int col = 0; col < board.get_cols(); ++col)
        {
            int shape_row = row - tetromino_y;
            int shape_col = col - tetromino_x;

            if (shape_row >= 0 && shape_row < shape.size() &&
                shape_col >= 0 && shape_col < shape[shape_row].size() &&
                shape[shape_row][shape_col] == tetris_block::FULL)
            {
                std::cout << "[X]";
            }
            else
            {
                std::cout << (board.is_cell_empty(row, col) ? "[ ]" : "[#]");
            }
        }
        std::cout << std::endl;
    }
}

std::mutex &tetris::get_mutex()
{
    return mutex;
}

std::condition_variable &tetris::get_condition_variable()
{
    return condition_variable;
}

// Place block on the board
void tetris::place_block(int row, int col) { board.place_block(row, col); }

bool tetris::is_game_over() const { return game_over; }

void tetris::set_game_over(bool game_state) { game_over = game_state; }

int tetris::get_game_speed() const { return game_speed; }

void tetris::set_game_speed(int speed) { game_speed = speed; }
