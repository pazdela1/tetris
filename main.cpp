#include <iostream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <random>
#include <mutex>
#include <condition_variable>
#include <thread>

#include "tetris.hpp"

void set_raw(bool set)
{
    if (set)
    {
        system("stty raw"); // enable raw
    }
    else
    {
        system("stty -raw"); // disable raw
    }
}

int main(int argc, char *argv[])
{
    tetris game;
    int speed = DEFAULT_SPEED; // 1s for game update by default

    for (int i = 1; i < argc; ++i)
    {
        std::string arg = argv[i];
        if (arg == "--help")
        {
            std::cout << "Hra Tetris\n\n";
            std::cout << "Ovládání:\n";
            std::cout << "`A` - posunutí dílku doleva\n";
            std::cout << "`D` - posunutí dílku doprava\n";
            std::cout << "`S` - posunutí dílku dolů\n";
            std::cout << "`W` - rotace dílku po směru hodinových ručiček:\n\n";
            std::cout << "Možnosti spuštění pro jinou obtížnost:\n";
            std::cout << "lehká obtížnost: tetris --easy\n";
            std::cout << "střední obtížnost: tetris --medium\n";
            std::cout << "těžká obtížnost: tetris --hard\n";
            std::cout << "velmi těžká obtížnost: tetris --impossible\n\n";
            std::cout << "Více informací naleznete v README.md\n";

            return 0;
        }

        // Game difficulty arguments
        if (arg == "--easy")
        {
            speed = 1500;
        }
        else if (arg == "--medium")
        {
            speed = 1000;
        }
        else if (arg == "--hard")
        {
            speed = 500;
        }
        else if (arg == "--impossible")
        {
            speed = 250;
        }
        else if (arg == "--testing")
        {
            // Comment out parts which to test and not to test

            std::cout << std::endl;
            std::cout << "\n\nVisual test of rotation" << std::endl;
            for (int i = 0; i < 4; ++i)
            {
                game.rotate();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test of left movement and collision" << std::endl;
            for (int i = 0; i < 10; ++i)
            {
                game.move_left();
                game.display_game_state();
                std::cout << std::endl;
            }
            /*
            std::cout << "\n\nVisual test of left kick-back after rotation" << std::endl;
            for (int i = 0; i < 3; ++i)
            {
                game.rotate();
                game.move_left();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test of right movement and collision" << std::endl;
            for (int i = 0; i < 10; ++i)
            {
                game.move_right();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test of right kick-back after rotation" << std::endl;
            for (int i = 0; i < 3; ++i)
            {
                game.rotate();
                game.move_right();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test of downward movement and collision" << std::endl;
            for (int i = 0; i < 20; ++i)
            {
                game.move_down();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test of downward kick-back after rotation" << std::endl;
            for (int i = 0; i < 3; ++i)
            {
                game.rotate();
                game.move_down();
                game.display_game_state();
                std::cout << std::endl;
            }

            std::cout << "\n\nVisual test for game over check" << std::endl;
            for (int i = 0; i < 100; ++i)
            {
                game.update();
                game.display_game_state();
                std::cout << std::endl;
            }
            */

            return 0;
        }
    }

    game.set_game_speed(speed);

    // Lambda for game logic thread
    auto game_logic_thread = [&game]()
    {
        while (!game.is_game_over())
        {
            game.update();
            std::this_thread::sleep_for(std::chrono::milliseconds(game.get_game_speed())); // Game logic update rate
        }
    };

    // Lambda for output thread
    auto output_thread = [&game]()
    {
        while (!game.is_game_over())
        {
            game.output();
        }
    };

    // Lambda for input thread
    auto input_thread = [&game]()
    {
        while (!game.is_game_over())
        {
            game.input();
        }
    };

    // Switch to raw mode
    set_raw(true);

    // Create threads
    std::thread logic_thread(game_logic_thread);
    std::thread render_thread(output_thread);
    std::thread user_input_thread(input_thread);

    // Join threads
    logic_thread.join();
    render_thread.join();
    user_input_thread.join();

    // Switch out of raw mode
    set_raw(false);

    return 0;
}
