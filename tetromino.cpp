#include "tetromino.hpp"

// __________TETROMINO_DEFINITIONS__________

// Set dimensions according to tetromino type
void tetromino::set_dimensions()
{
    switch (type)
    {
    case tetromino_type::I_BLOCK:
        width = height = 4;
        break;
    case tetromino_type::O_BLOCK:
        width = height = 2;
        break;
    default:
        width = height = 3;
    }
}

// Set starting position to the middle
void tetromino::set_starting_position()
{
    switch (type)
    {
    case tetromino_type::O_BLOCK:
        board_x = 4;
        break;
    case tetromino_type::I_BLOCK:
    case tetromino_type::T_BLOCK:
    case tetromino_type::S_BLOCK:
    case tetromino_type::Z_BLOCK:
    case tetromino_type::J_BLOCK:
    case tetromino_type::L_BLOCK:
        board_x = 3;
        break;
    }
}

// Fill the cells according to tetromino type
void tetromino::initialize_shape()
{
    // Clearing any previous shape
    for (auto &row : shape)
    {
        std::fill(row.begin(), row.end(), tetris_block::EMPTY);
    }

    switch (type)
    {
    case tetromino_type::I_BLOCK:
        shape[1][0] = shape[1][1] = shape[1][2] = shape[1][3] = tetris_block::FULL;
        break;
    case tetromino_type::O_BLOCK:
        shape[0][0] = shape[0][1] = shape[1][0] = shape[1][1] = tetris_block::FULL;
        break;
    case tetromino_type::T_BLOCK:
        shape[0][1] = shape[1][0] = shape[1][1] = shape[1][2] = tetris_block::FULL;
        break;
    case tetromino_type::S_BLOCK:
        shape[0][1] = shape[0][2] = shape[1][0] = shape[1][1] = tetris_block::FULL;
        break;
    case tetromino_type::Z_BLOCK:
        shape[0][0] = shape[0][1] = shape[1][1] = shape[1][2] = tetris_block::FULL;
        break;
    case tetromino_type::J_BLOCK:
        shape[0][0] = shape[1][0] = shape[1][1] = shape[1][2] = tetris_block::FULL;
        break;
    case tetromino_type::L_BLOCK:
        shape[0][2] = shape[1][0] = shape[1][1] = shape[1][2] = tetris_block::FULL;
        break;
    }
}

// Rotate the shape
void tetromino::rotate()
{
    for (int i = 0; i < height; ++i)
    {
        for (int j = i; j < width; ++j)
        {
            std::swap(shape[i][j], shape[j][i]);
        }
    }
    for (auto &row : shape)
    {
        std::reverse(row.begin(), row.end());
    }
}

// Rotate the shape back
void tetromino::tetromino::rotate_back()
{
    for (int i = 0; i < height; ++i)
    {
        for (int j = i; j < width; ++j)
        {
            std::swap(shape[i][j], shape[j][i]);
        }
    }
    for (int col = 0; col < width; ++col)
    {
        for (int i = 0; i < height / 2; ++i)
        {
            std::swap(shape[i][col], shape[height - i - 1][col]);
        }
    }
}

// Display the tetromino
void tetromino::display_tetromino() const
{
    for (const auto &row : shape)
    {
        for (auto cell : row)
        {
            if (cell == tetris_block::FULL)
            {
                std::cout << "[X]";
            }
            else
            {
                std::cout << "[ ]";
            }
        }
        std::cout << std::endl;
    }
}

const std::vector<std::vector<tetris_block>> &tetromino::get_shape() const { return shape; }

tetromino_type tetromino::get_type() const { return type; }

int tetromino::get_board_x() const { return board_x; }
void tetromino::set_board_x(int new_x) { board_x = new_x; }

int tetromino::get_board_y() const { return board_y; }
void tetromino::set_board_y(int new_y) { board_y = new_y; }

int tetromino::get_width() const { return width; }
int tetromino::get_height() const { return height; }
