#ifndef TETRIS_TETRIS_BOARD_HPP
#define TETRIS_TETRIS_BOARD_HPP

#include <iostream>

#include "tetromino.hpp"

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 20

class tetris_board
{
private:
    std::vector<std::vector<tetris_block>> board;
    int rows, cols;

public:
    tetris_board();

    void place_block(int row, int col);

    bool is_cell_empty(int row, int col) const;

    bool full_line(int row) const;
    void clear_line(int row);
    int clear_full_lines();

    void display_board() const;

    const std::vector<std::vector<tetris_block>> &get_board() const;

    int get_rows() const;
    int get_cols() const;
};

#endif // TETRIS_TETRIS_BOARD_HPP
